"use strict";

var Application = function() {};

Application.prototype = {

  initGame: function() {
    this.score = 0;
    this.correct = 0;
    this.quizID = "0";
    this.category = "0";
    this.descript = "0";
    this.answer = "";

    this.getJSONData();
  },

  getJSONData: function() {
    // let targetURL = "https://jservice.io/api/random";
    let targetURL = "https://qukz-back-2.herokuapp.com/questions/random";
    let self = this;
    let request = new XMLHttpRequest();

    request.open("GET", targetURL, true);

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        let data = JSON.parse(request.responseText);

        self.setData(data);
        self.render();
        self.skipBtnEvent();
        self.symbolEvent();
      } else {
        console.log("server not respond");
      }
    }
    request.send();
  },

  skipBtnEvent: function() {
    let self = this;
    let skipBtn = document.getElementById("skip-btn");
    let score = document.getElementById("total-counter");

    skipBtn.onclick = function(e) {
      self.score += 1;
      score.innerHTML = self.score;

      self.wait();
      self.clearArea();
      self.getJSONData();
    };
  },

  render: function() {
    let id = document.getElementById("question-id");
    id.innerHTML = this.quizID;

    let cat = document.getElementById("category-value");
    cat.innerHTML = this.category;

    let des = document.getElementById("description-text");
    des.innerHTML = this.descript;

    this.renderAnswer();
  },

  setData: function(data) {
    this.quizID = data.id;
    this.category = data.category.title;
    this.descript = data.description;
    this.answer = data.answer;

    //until better times
    // this.quizID = data[0].id;
    // this.category = data[0].category.title;
    // this.descript = data[0].question;
    // this.answer = data[0].answer;
    //Remove latter
    console.log(this.answer);
  },

  renderAnswer: function() {
    let arr = this.answer.split("");

    arr = shuffle(arr);
    let symbols = document.getElementById("symbols-list");
    let li = null;

    for (let i = 0; i < arr.length; i++) {
      li = document.createElement("li");
      li.className = "symbol-list-li";
      li.innerHTML = arr[i];
      if (arr[i] == " ") {
        li.classList.add("symbol-space");
      }

      symbols.appendChild(li);
    }

    function shuffle(arr) {
      let crr = arr.length,
        temp, rand;

      while (0 !== crr) {

        rand = Math.floor(Math.random() * crr);
        crr -= 1;

        temp = arr[crr];
        arr[crr] = arr[rand];
        arr[rand] = temp;
      }

      return arr;
    }
    symbols.appendChild(li);
  },

  clearArea: function() {
    // let symbols = document.getElementsByClassName("symbol-list-li");
    let ulAnswer = document.getElementById("answers-list");
    let ulSymbols = document.getElementById("symbols-list");
    let arr = [],
      length = 0;

    arr = ulSymbols.children;
    length = ulSymbols.children.length;
    for (let i = length; i > 0; i--) {
      ulSymbols.removeChild(arr[i - 1]);
    }

    arr = ulAnswer.children;
    length = ulAnswer.children.length;
    for (let i = length; i > 0; i--) {
      ulAnswer.removeChild(arr[i - 1]);
    }
  },

  symbolEvent: function() {
    let self = this;
    let arrSymbols = document.getElementsByClassName("symbol-list-li");
    let symbols = document.getElementById("symbols-list");
    let answers = document.getElementById("answers-list");

    for (let i = 0; i < arrSymbols.length; i++) {
      arrSymbols[i].onclick = toArea;
    }

    function toArea(evt) {
      if (evt.target.parentNode == symbols) {

        symbols.removeChild(evt.target);
        answers.appendChild(evt.target);

      } else if (evt.target.parentNode == answers) {

        answers.removeChild(evt.target);
        symbols.appendChild(evt.target);

        if (symbols.children.length == 1) {
          self.wait();
        }
      }

      if (symbols.children.length == 0) {
        self.check(answers);
      }
    }
  },

  check: function(answers) {
    let self = this;
    let answer = symbolsToString(answers);
    let arrSymbols = document.getElementById("symbols-list");
    let area = document.getElementsByClassName("answer-area")[0];
    let symbolArea = document.getElementsByClassName("symbols-area")[0];

    let result = document.getElementById("answer-result");
    if (arrSymbols.children.length == 0) {
      if (answer == this.answer) {
        win(result);
      } else {
        lose(result);
      }
    }

    function symbolsToString(answers) {
      let str = "";

      for (let i = 0; i < answers.children.length; i++) {
        str += answers.children[i].textContent || answers[i].children.innerText;
      }

      return str;
    }

    function win(result) {
      result.innerHTML = "Correct answer!";

      if (result.classList) {
        result.classList.add("correct-answer");
      } else {
        result.className += " " + "correct-answer";
      }
      result.style.display = "block";

      if (area.classList) {
        area.classList.add("win-answer-area");
      } else {
        area.className += " " + "win-answer-area";
      }

      symbolArea.style.display = "none";

      let winBtn = document.createElement("div");
      winBtn.innerHTML = "NEXT QUESTION";
      winBtn.id = "win-btn";

      area.parentNode.insertBefore(winBtn, area.nextSibling);

      let score = document.getElementById("total-counter");
      let correct = document.getElementById("correct-counter");
      winBtn.onclick = function(e) {
        self.score += 1;
        score.innerHTML = self.score;

        self.correct += 1;
        correct.innerHTML = self.correct;

        self.wait();
        self.clearArea();
        self.getJSONData();
      }
    }

    function lose(result) {
      result.innerHTML = "Damn!";
      result.classList.add("incorrect-answer");
      result.style.display = "block";

      if (area.classList) {
        area.classList.add("lose-answer-area");
      } else {
        area.className += " " + "lose-answer-area";
      }

      symbolArea.style.display = "none";
    }
  },

  wait: function() {
    let result = document.getElementById("answer-result");
    result.style.display = "none";
    result.classList.remove("correct-answer");
    result.classList.remove("incorrect-answer");

    let area = document.getElementsByClassName("answer-area")[0];
    area.classList.remove("win-answer-area");
    area.classList.remove("lose-answer-area");
    area.classList.add("wait-answer-area");

    let winBtn = document.getElementById("win-btn");
    let app = document.getElementsByClassName("application")[0];
    if (winBtn != undefined) app.removeChild(winBtn);

    let symbolArea = document.getElementsByClassName("symbols-area")[0];
    symbolArea.style.display = "block";
  }
}

let newApplication = new Application();
newApplication.initGame();
